# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = '邵阳七中2201班'
copyright = '2023, XiangQinxi'
author = 'XiangQinxi'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser",
    "sphinx_design",

    "sphinx.ext.autodoc",
    "sphinx.ext.githubpages",

    "sphinx.ext.intersphinx",

    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.todo',
    'sphinx.ext.autosectionlabel',

]

templates_path = ['_templates']
exclude_patterns = []

language = 'zh_CN'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'pydata_sphinx_theme'
html_static_path = ['_static']

html_theme_options = {

    "navbar_start": ["navbar-logo"],
    "navbar_center": ["navbar-nav"],
    "navbar_end": ["navbar-icon-links"],
    "navbar_persistent": ["search-button"],

    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.com/XiangQinxi/shaoyang-7.2201",
            "icon": "fa-brands fa-square-gitlab",
            "type": "fontawesome",
        },
    ],

    "header_links_before_dropdown": 4,
}
