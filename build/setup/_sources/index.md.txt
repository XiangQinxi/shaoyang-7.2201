# 湖南省邵阳市第七中学 2201班

```{eval-rst}
.. toctree::
   :maxdepth: 2
   :hidden:
   :glob:
   
   blog/*
   log/*
   data/*
```

此文档为`七中2201班`所编辑，目的在于记录`2201班`所发生过的事，以留做纪念

```{hint}
创建者：向秦希

如需要加入其他文档，请联系我并带上文本，我会进行整理上传。
```


```{eval-rst}


.. card:: 多页面版本
    :link: https://sy7ms-2201.netlify.app/


.. card:: 单页面版本
    :link: https://sy7ms-2201.netlify.app/singlehtml/


```

---

## 所有博客


```{eval-rst}

.. card:: Blog 2023.4.5
    :link: blog/blog-2023.4.5.html
    
    正式构建发布

```

## 所有记录


```{eval-rst}

.. card:: Log 2022.12.13
    :link: log/log-2022.12.13.html
    
    班级群①成立
    
.. card:: Log Log 2023.3.13
    :link: log/log-2023.3.13.html
    
    班级群②成立
        
.. card:: Log 2023.2 - 2023.2.18
    :link: log/log-2023.2.28.html
    
    记录七中成立
    
```

## 所有数据


```{eval-rst}

.. card:: Data 2023.4.5
    :link: data/data-2023.4.5.html
    
    七中（原十五中）信息
    
```